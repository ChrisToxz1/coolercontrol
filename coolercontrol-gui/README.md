# coolercontrol-gui

is the frontend UI, client of the daemon program, and main "coolercontrol" program and desktop application.
It is written in Python and uses PySide6 for the UI framework. Its dependencies and a python interpreter are embedded in the executable,
negating any system-level python dependencies.
